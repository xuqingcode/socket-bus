package site.xuqing.app1;

import androidx.appcompat.app.AppCompatActivity;
import site.xuqing.nobean.obj.INonObject;
import site.xuqing.socketbus.SocketBus;
import site.xuqing.socketbus.core.SocketClient;
import site.xuqing.socketbus.listener.OnMsgReceive;
import site.xuqing.socketbus.uitls.NoBeanUtil;
import site.xuqing.socketbus.uitls.NoBeanUtil.Route;
import site.xuqing.socketbus.uitls.RouteUtil;

import android.Manifest.permission;
import android.os.Bundle;

import com.tbruyelle.rxpermissions3.RxPermissions;

public class MainActivity extends AppCompatActivity {
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        final RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.request(permission.READ_EXTERNAL_STORAGE, permission.WRITE_EXTERNAL_STORAGE)
            .subscribe(granted -> {});
        
        try {
            client();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public  void client() throws Exception{
        SocketClient socketClient=SocketBus.getInstance().clientSocketServer(this, new OnMsgReceive() {
            @Override
            public void onMsg(INonObject msg) {
                System.out.println(">>>>>>>>>>>>>>>App1:"+msg.getValue("name"));
            }
        });
        Thread.sleep(1000);
        socketClient.getClientSendHandler().sendMsg(new Route(RouteUtil.getRoute(this),"site.xuqing.app.MainActivity"), NoBeanUtil.getTestEvent("你好，App,我是App1发来的消息"));
    }
}