package site.xuqing.socketbus.core.queue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import site.xuqing.nobean.obj.INonObject;

/**
 * @author xuqing
 * @Description EventQueue
 * @createTime 2022年01月26日 08:53:54
 */
public class EventQueue {
    private final Map<String, List<INonObject>> queue = new HashMap<>();
    
    public void inQueue(String route, INonObject msg) {
        if (queue.containsKey(route)) {
            List<INonObject> msgList = queue.get(route);
            if (msgList == null) {
                msgList = new ArrayList<>();
            }
            msgList.add(msg);
            queue.replace(route, msgList);
        } else {
            List<INonObject> msgList = new ArrayList<>();
            msgList.add(msg);
            queue.put(route, msgList);
        }
    }
    
    public List<INonObject> outQueue(String route) {
        if (queue.containsKey(route)) {
            List<INonObject> msgList=queue.get(route);
            queue.remove(route);
            return msgList;
        }
        return new ArrayList<>();
    }
    
    public void clear() {
        queue.clear();
    }
}
