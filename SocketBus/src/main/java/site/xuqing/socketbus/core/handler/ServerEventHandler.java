package site.xuqing.socketbus.core.handler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import site.xuqing.nobean.NoBean;
import site.xuqing.nobean.obj.INonObject;
import site.xuqing.nobean.obj.NonObject;
import site.xuqing.socketbus.core.SocketServer;
import site.xuqing.socketbus.uitls.NoBeanUtil.EventType;

/**
 * @author xuqing
 * @Description 服务的接收消息线程
 * @createTime 2022年01月21日 14:53:56
 */
public class ServerEventHandler extends Thread {
    private final Socket socket;
    private final SocketServer mSocketServer;
    
    public ServerEventHandler(Socket socket, SocketServer socketServer) {
        this.socket = socket;
        this.mSocketServer = socketServer;
    }
    
    @Override
    public void run() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String msg;
            while ((msg = bufferedReader.readLine()) != null) {
                System.out.println("##服务的转发消息：" + msg);
                NonObject nonObject = NoBean.formJson(msg);
                //判断消息类型
                switch (EventType.valueOf(nonObject.getValue("type"))) {
                    case BIND:
                        String route=nonObject.getObject("data").getValue("bindRoute");
                        mSocketServer.setRoutePort(route, nonObject.getObject("data").getValue("port"));
                        //这里获取消息队列消息，如果有消息，则直接回调给用户
                        List<INonObject> msgList=mSocketServer.outQueue(route);
                        if (msgList!=null&&msgList.size()>0){
                            PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
                            for (INonObject object:msgList) {
                                printWriter.println(NoBean.toJson(object));
                            }
                        }
                        break;
                    case MSG:
                        //转发给指定route的客户端
                        String targetRoute = nonObject.getObject("route").getValue("targetRoute");
                        Integer targetSocketPort=mSocketServer.getRoutePort(targetRoute);
                        if (targetSocketPort==null){
                            mSocketServer.inQueue(targetRoute,nonObject);
                        }else {
                            Socket targetSocket = mSocketServer.getSocket(targetSocketPort);
                            PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(targetSocket.getOutputStream()), true);
                            printWriter.println(msg);
                        }
                        break;
                    case QUIT:
                        System.out.println("用户主动关闭连接，连接关闭");
                        try {
                            mSocketServer.removeSocket(socket.getLocalPort());
                            mSocketServer.removeRoutePort(nonObject.getObject("data").getValue("route"));
                            socket.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("连接异常断开");
        } finally {
            System.out.println("连接关闭");
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
