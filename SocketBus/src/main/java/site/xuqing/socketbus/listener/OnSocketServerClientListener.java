package site.xuqing.socketbus.listener;

/**
 * @author xuqing
 * @Description 服务器开启监听
 * @createTime 2022年01月24日 13:09:40
 */
public interface OnSocketServerClientListener {
    /**
     * 成功
     */
    void onSuccess();
    
    /**
     * 错误，主要指端口被占用
     */
    void onError();
}
