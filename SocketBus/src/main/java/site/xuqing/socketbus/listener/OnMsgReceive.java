package site.xuqing.socketbus.listener;

import site.xuqing.nobean.obj.INonObject;

/**
 * @author xuqing
 * @Description 消息接收监听器
 * @createTime 2022年01月24日 15:00:57
 */
public interface OnMsgReceive {
    /**
     * 接收消息
     * @param msg 用户发送的消息类
     */
    void onMsg(INonObject msg);
}
