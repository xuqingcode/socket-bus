package site.xuqing.socketbus.uitls;

import android.app.Activity;

import androidx.fragment.app.Fragment;

/**
 * @author xuqing
 * @Description TODO
 * @createTime 2022年01月26日 11:39:33
 */
public class RouteUtil {
    public static String getRoute(Activity activity) {
        return getRoute((Object) activity);
    }
    
    public static String getRoute(Fragment fragment) {
        return getRoute((Object) fragment);
    }
    
    public static String getRoute(Object object) {
        return object.getClass().getCanonicalName();
    }
}
