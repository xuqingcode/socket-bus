package site.xuqing.socketbus.uitls;

import androidx.annotation.NonNull;
import site.xuqing.nobean.obj.INonObject;
import site.xuqing.nobean.obj.NonObject;

/**
 * @author xuqing
 * @Description 消息帮助类
 * @createTime 2022年01月25日 14:27:41
 */
public class NoBeanUtil {
    public static INonObject getBaseEvent(Route route,@NonNull EventType type) {
        NonObject nonObject = new NonObject();
        if (route!=null) {
            NonObject routeObject=new NonObject();
            routeObject.setValue("inRoute", route.inRoute);
            routeObject.setValue("targetRoute", route.targetRoute);
            nonObject.setObject("route",routeObject);
        }
        nonObject.setValue("type", type.name());
        return nonObject;
    }
    
    public static INonObject getBindEvent(int port,@NonNull String bindRoute) {
        NonObject nonObject = new NonObject();
        nonObject.setValue("port", port);
        nonObject.setValue("bindRoute", bindRoute);
        return nonObject;
    }
    
    public static INonObject getTestEvent(@NonNull String name) {
        NonObject nonObject = new NonObject();
        nonObject.setValue("name", name);
        return nonObject;
    }
    
    public static class Route {
        private String inRoute;
        private String targetRoute;
        
        public Route(@NonNull String inRoute, @NonNull String targetRoute) {
            this.inRoute = inRoute;
            this.targetRoute = targetRoute;
        }
        
        public String getInRoute() {
            return inRoute;
        }
        
        public void setInRoute(String inRoute) {
            this.inRoute = inRoute;
        }
        
        public String getTargetRoute() {
            return targetRoute;
        }
        
        public void setTargetRoute(String targetRoute) {
            this.targetRoute = targetRoute;
        }
    }
    
    public enum EventType {
        /**
         * 绑定消息
         */
        BIND,
        /**
         * 信息消息
         */
        MSG,
        /**
         * 退出消息
         */
        QUIT
    }
}
