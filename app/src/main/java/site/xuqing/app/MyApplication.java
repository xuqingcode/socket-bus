package site.xuqing.app;

import android.app.Application;

import site.xuqing.socketbus.SocketBus;

/**
 * @author xuqing
 * @Description TODO
 * @createTime 2022年01月21日 10:04:36
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SocketBus.getInstance().initSocketServer();
    }
}
