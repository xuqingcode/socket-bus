# SocketBus

#### 介绍
利用socket搭建的Android通讯框架，可app间通讯，可跨进程通讯，可跨app通讯

#### 软件架构
- 主要采用的是Socket搭建的c/s架构。
- s端开启一个Socket服务，绑定一个port，主要用于：
    1. 消息的接收与分发。
    2. 连接的管理。
    3. 消息的缓存。
- c端是Socket客户端，自动创建一个port，并连接到Socket服务,主要用于：
    1. 消息的发送。
    2. 消息的接收。
    3. 消息对象的封装。

#### 安装教程

1.  在项目的gredle文件中添加：

```groovy
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```

2.  在app的gredle文件中添加：
```groovy
dependencies {
    implementation 'com.gitee.xuqingcode:socket-bus:1.0.0'
}
```
3.  在app的主清单文件AndroidManifest.xml中添加权限：
```xml
<uses-permission android:name="android.permission.INTERNET"/>
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
```
注意，存储权限需要动态申请。至此，SocketBus便安装完成！
#### 使用说明
1.  动态申请存储权限完成后注册SocketBus服务：
```java
SocketBus.getInstance().initSocketServer();
```
2.  使用SocketBus：
```java
SocketClient socketClient=SocketBus.getInstance().clientSocketServer(this, new OnMsgReceive() {
     @Override
     public void onMsg(INonObject msg) {
          //接收到消息后处理消息
     }
});
```
3.  使用SocketBus发送消息：
```java
socketClient.getClientSendHandler().sendMsg(new Route(RouteUtil.getRoute(this),"site.xuqing.app1.MainActivity"), NoBeanUtil.getTestEvent("你好，App1,我是App发来的消息"));
```
详情可参考项目中的例子！
#### 最后
感谢各位读者，对项目感兴趣，如遇问题，可对项目进行指正！
